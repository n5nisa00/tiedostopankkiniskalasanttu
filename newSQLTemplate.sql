drop database if exists tiedostopankki;
create database tiedostopankki;
use tiedostopankki;

create table tiedosto (
    id int primary key auto_increment,
    nimi varchar(255) not null,
    tiedostonimi varchar(255) not null,
    kuvaus varchar(255),
    tallennettu timestamp default current_timestamp
    on update current_timestamp
);