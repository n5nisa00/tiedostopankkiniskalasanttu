<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->helper('directory');
    }
    
    public function hae_kaikki($limit=NULL, $offset=NULL){
        
        $this->db->limit($limit, $offset);
        
        $query=$this->db->get('tiedosto');
        return $query->result();
    }
    
    public function hae($id){
        $this->db->where('id', $id);
        $query = $this->db->get('tiedosto');
        return $query->row();
    }
    
    public function laske_tiedostot() {
        return $this->db->count_all_results('tiedosto');
    }
    
    public function lisaa($data) {
        //luodaan tiedoston polku
        $polku = $this->config->item('upload_path') . '/';
        
        try {
            $tiedosto_nimi = $this->tallenna_tiedosto($polku);
            $data['tiedostonimi'] = $tiedosto_nimi;
            //lisää tietokantaan
            $this->db->insert('tiedosto', $data);
            redirect("tiedosto/index");
        } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
    
    private function tallenna_tiedosto($polku) {
        //asetetaan uploadin asetukset
        $config['upload_path'] = $polku;
        $config['allowed_types'] = '*';
        $config['max_size'] = '10000';
        $config['max_width'] = '3000';
        $config['max_height'] = '2000';
        
        $this->load->library('upload',$config);
        
        //yritetään uploadata
        if (!$this->upload->do_upload()) {
            throw new Exception ($this->upload->display_errors());
        } else {
            $tiedoston_tiedot = $this->upload->data();
            return $tiedoston_tiedot['file_name'];
        }
    }
    
    public function poista($id) {
        //haetaan tiedoston tiedot tietokannasta id:n avulla
        $tiedosto = $this->hae($id);
        
        //luodaan polku poistettavaan tiedostoon
        $polku = $this->config->item("upload_path") . "/$tiedosto->tiedostonimi";
        //poistetaan tiedosto
        unlink($polku);
        
        //poistetaan tiedoston tiedot tietokannasta
        $this->db->where ('id', $id);
        $this->db->delete('tiedosto');
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

