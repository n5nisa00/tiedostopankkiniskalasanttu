<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('tiedosto_model');
        $this->load->library('pagination');
    }

    public function index()
    {
        $tiedostoja_sivulla = 3;
        
        $data['tiedostot'] = $this->tiedosto_model->hae_kaikki($tiedostoja_sivulla,$this->uri->segment(3));

        $data['sivutuksen_kohta'] = 0;
        if ($this->uri->segment(3)) {
            $data['sivutuksen_kohta'] = $this->uri->segment(3);
        }

        $config['base_url'] = site_url('tiedosto/index');
        $config['total_rows'] = $this->tiedosto_model->laske_tiedostot();
        $config['per_page'] = $tiedostoja_sivulla;
        $config['uri_segment'] = 3; 

        $this->pagination->initialize($config);
     
        $data['main_content'] = 'tiedostot_view';
        $this->load->view('template', $data);
    }
    
    public function lataa_tiedosto() {
            //haetaan annetut tiedot tiedostosta
            $data = array (
                'nimi' => $this->input->post('nimi'),
                'kuvaus' => $this->input->post('kuvaus'),
                'tiedostonimi' => ''
            );
            
            //jos tiedostolle on annettu nimi, niin uploadataan se. muulloin ladataan sivu uudestaan
            if (!empty($data['nimi'])) {
                $this->tiedosto_model->lisaa($data);
            } else {
                redirect("tiedosto/lisaa");
            }
        }
    
    public function lisaa(){
        $this->load->helper('form');
        $data['main_content'] = 'tiedosto_view';
        $this->load->view('template', $data);
    }
    
    public function poista_tiedosto($id) {
            $this->tiedosto_model->poista($id);
            //$this->index();
            redirect('/tiedosto/index');
    }
}
