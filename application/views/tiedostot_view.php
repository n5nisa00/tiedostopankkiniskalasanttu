<h4 style="display:inline-block;">Tiedostot</h4>
<!--<a class="btn btn-primary pull-right" href="<?php site_url() . '/tiedosto/lisaa'?>" role="button">Lisää</a>-->
<?php print anchor('tiedosto/lisaa', 'Lisää','class="btn btn-primary pull-right"');?>
<hr>
<table class="table">
    <thead>
    <tr>
        <th>Nimi</th>
        <th>Tiedosto</th>
        <th>Kuvaus</th>
        <th>Tallennettu</th>
        <th></th>
    </tr>
    </thead>
        <tbody>
        <?php
            foreach ($tiedostot as $tiedosto) {
                print "<tr>";
                print '<td>' . anchor(site_url() . 'uploads/' . $tiedosto->tiedostonimi, $tiedosto->nimi) . '</td>';
                print '<td>' . anchor(site_url() . 'uploads/' . $tiedosto->tiedostonimi, $tiedosto->tiedostonimi) . '</td>';
                print "<td>$tiedosto->kuvaus</td>";
                print '<td>' . date('d.m.Y H.i', strtotime($tiedosto->tallennettu)) . '</td>';
                print '<td>' . anchor("tiedosto/poista_tiedosto/$tiedosto->id", 'Poista') . '</td>';
                print "</tr>";
            }
        ?>
        </tbody>
    </table>

<?php echo $this->pagination->create_links(); ?>